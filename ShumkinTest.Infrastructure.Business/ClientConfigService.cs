using Microsoft.Extensions.Configuration;

using ShumkinTest.Domain.Core.Extensions;
using ShumkinTest.Domain.Core.Models.ConfigModels;

namespace ShumkinTest.Infrastructure.Business
{
	public class ClientConfigService
	{
		private readonly IConfiguration _configuration;
		private readonly ServerConfig _serverConfig;

		public ClientConfigService(string[] args)
		{
			_configuration = SetConfiguration(args);
			_serverConfig = GetServerConfig();
		}

		private IConfiguration SetConfiguration(string[] args)
		{
			if (_configuration == null)
			{
				var builder = ConfigHelper.GetConfigurationBuilder(args);
				return builder.Build();
			}

			return _configuration;
		}

		public IConfiguration GetConfiguration()
		{
			return _configuration;
		}

		public ServerConfig GetServerConfig()
		{
			return _serverConfig ?? _configuration.GetServerConfig();
		}
	}
}