using Microsoft.Extensions.Configuration;

using ShumkinTest.Domain.Core.Extensions;

namespace ShumkinTest.Infrastructure.Business
{
	public class ServerConfigService
	{
		private readonly IConfiguration _configuration;

		public ServerConfigService(string[] args)
		{
			_configuration = SetConfiguration(args);
		}

		private IConfiguration SetConfiguration(string[] args)
		{
			if (_configuration == null)
			{
				var builder = ConfigHelper.GetConfigurationBuilder(args);
				return builder.Build();
			}

			return _configuration;
		}

		public IConfiguration GetConfiguration()
		{
			return _configuration;
		}
	}
}