using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using ShumkinTest.Domain.Core.Entities.Client;
using ShumkinTest.Domain.Interfaces;
using ShumkinTest.Infrastructure.Data.LocalDb.Repositories;
using ShumkinTest.Services.Interfaces;

namespace ShumkinTest.Infrastructure.Business
{
	public class MessageSyncService : ISyncService<ClientMessage>
	{
		private readonly ClientMessageRepo _clientMessageRepo;
		private readonly IServerAPIService<ClientMessage> _serverApiService;

		public MessageSyncService(IRepository<ClientMessage> clientMessageRepo, IServerAPIService<ClientMessage> serverApiService)
		{
			_clientMessageRepo = (ClientMessageRepo)clientMessageRepo;
			_serverApiService = serverApiService;
		}

		public async Task StartSync()
		{
			Debug.Write("Start syncing\n");
			var notSynced = await GetNotSynced().ToListAsync();
			foreach (var message in notSynced) await Task.Run(() => TrySyncAsync(message));
			Debug.Write("End syncing\n");
		}

		public IQueryable<ClientMessage> GetNotSynced()
		{
			return _clientMessageRepo.GetListQuery().Where(p => !p.IsSync);
		}

		public async Task<bool> TrySyncAsync(ClientMessage entity)
		{
			var result = await _serverApiService.SendEntity(entity);
			if (result)
			{
				var localDbResult = await _clientMessageRepo.SetSynced(entity);
				Debug.Write($"Message: {entity.Id} synced\n");
				return localDbResult;
			}

			return false;
		}
	}
}