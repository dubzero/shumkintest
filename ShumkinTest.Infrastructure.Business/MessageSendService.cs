using System.Collections.Generic;
using System.Threading.Tasks;

using Flurl;
using Flurl.Http;

using ShumkinTest.Domain.Core.Entities.Client;
using ShumkinTest.Domain.Core.Models;
using ShumkinTest.Domain.Interfaces;
using ShumkinTest.Services.Interfaces;

namespace ShumkinTest.Infrastructure.Business
{
	public class MessageSendService : IServerAPIService<ClientMessage>
	{
		private readonly ClientConfigService _config;

		public MessageSendService(ClientConfigService config)
		{
			_config = config;
		}

		public async Task<bool> SendEntity(ClientMessage entity)
		{
			var messageModel = new MessageCreateModel(entity);

			var response = await _config.GetServerConfig().Url
				.AppendPathSegment("/api/message")
				.PostJsonAsync(messageModel);

			return response.StatusCode == 200;
		}

		public async Task<IEnumerable<IViewModel>> GetEntities()
		{
			return await _config.GetServerConfig().Url
				.AppendPathSegment("/api/message/all")
				.GetJsonAsync<IEnumerable<MessageViewModel>>();
		}
	}
}