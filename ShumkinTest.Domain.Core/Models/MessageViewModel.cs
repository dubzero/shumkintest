using System;
using System.Text.Json.Serialization;

using ShumkinTest.Domain.Interfaces;

namespace ShumkinTest.Domain.Core.Models
{
	public class MessageViewModel : IViewModel
	{
		[JsonPropertyName("text")]
		public string Text { get; set; }

		[JsonPropertyName("createDate")]
		public string CreateDate { get; set; }

		[JsonPropertyName("IPAddress")]
		public string IPAddress { get; set; }

		public string GetPrettyView()
		{
			return $"{CreateDate}\r | IP: {IPAddress} \r | {Text}";
		}

		public void PrettyWriteConsole()
		{
			Console.BackgroundColor = ConsoleColor.DarkGreen;
			Console.Write($"{CreateDate} \t");

			Console.BackgroundColor = ConsoleColor.Black;
			Console.Write(" | ");

			Console.ForegroundColor = ConsoleColor.Red;
			Console.Write($"IP : {IPAddress} \t");

			Console.ForegroundColor = ConsoleColor.White;
			Console.Write(" | ");
			Console.WriteLine(Text);
		}
	}
}