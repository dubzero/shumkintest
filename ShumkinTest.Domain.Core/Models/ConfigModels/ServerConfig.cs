using Microsoft.Extensions.Configuration;

namespace ShumkinTest.Domain.Core.Models.ConfigModels
{
	public class ServerConfig
	{
		public ServerConfig(IConfigurationSection section)
		{
			Url = section.GetSection("Url").Value;
			if (int.TryParse(section.GetSection("SyncTimeout").Value, out var timeout))
			{
				SyncTimeout = timeout;
			}
		}

		public string Url { get; set; }

		public int? SyncTimeout { get; set; }
	}
}