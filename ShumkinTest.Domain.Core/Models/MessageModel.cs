using ShumkinTest.Domain.Core.Entities.Client;

namespace ShumkinTest.Domain.Core.Models
{
	public class MessageCreateModel
	{
		public MessageCreateModel()
		{
		}

		public MessageCreateModel(ClientMessage message)
		{
			Text = message.Text;
		}

		public string Text { get; set; }
	}
}