using System;
using System.IO;

using Microsoft.Extensions.Configuration;

using ShumkinTest.Domain.Core.Models.ConfigModels;

namespace ShumkinTest.Domain.Core.Extensions
{
	public static class ConfigHelper
	{
		public static IConfigurationBuilder GetConfigurationBuilder(string[] args)
		{
			return new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddCommandLine(args)
				.AddEnvironmentVariables()
				.AddJsonFile("appsettings.json", false, true)
				.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true,
					true);
		}

		public static IConfigurationBuilder GetConfigurationBuilder(string[] args, string environment)
		{
			return new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddCommandLine(args)
				.AddEnvironmentVariables()
				.AddJsonFile("appsettings.json", false, true)
				.AddJsonFile($"appsettings.{environment}.json", true,
					true);
		}

		public static ServerConfig GetServerConfig(this IConfiguration configuration)
		{
			var options = new ServerConfig(configuration.GetSection("Server-Settings"));
			return options;
		}

		public static string GetModifiedConnectionString(this IConfiguration configuration, string name)
		{
			var connectionString = configuration.GetConnectionString(name);
			var username = Environment.GetEnvironmentVariable("db-username");
			if (string.IsNullOrEmpty(username))
			{
				username = SetDbUsername();
			}

			var password = Environment.GetEnvironmentVariable("db-password");
			if (string.IsNullOrEmpty(password))
			{
				password = SetDbPassword();
			}

			return connectionString.Replace("db-username", username).Replace("db-password", password);
		}

		/// <summary>
		/// Ввод пользователя бд из консоли
		/// </summary>
		/// <returns></returns>
		private static string SetDbUsername()
		{
			var username = "";

			while (string.IsNullOrEmpty(username))
			{
				username = ReadLine.Read("'db-username' env variable not set. Write PostgreSQL username manually: ");
				Environment.SetEnvironmentVariable("db-username", username);
			}

			return username;
		}

		/// <summary>
		/// Ввод пароля бд из консоли
		/// </summary>
		/// <returns></returns>
		private static string SetDbPassword()
		{
			var password = "";

			while (string.IsNullOrEmpty(password))
			{
				password = ReadLine.ReadPassword("'db-password' env variable not set. Write PostgreSQL password manually: ");
				Environment.SetEnvironmentVariable("db-password", password);
			}

			return password;
		}
	}
}