using System;
using System.Globalization;

using NodaTime;

namespace ShumkinTest.Domain.Core.Extensions
{
	public static class NodaTimeExtension
	{
		public static Instant CurrentTime => Instant.FromDateTimeUtc(DateTime.UtcNow);

		public static string InstantToString(this Instant? date, string format = "dd.MM.yyyy")
		{
			return !date.HasValue
				? ""
				: date.Value.ToString(format, CultureInfo.InvariantCulture);
		}

		public static string InstantToString(this Instant date, string format = "dd.MM.yyyy")
		{
			return date.ToString(format, CultureInfo.InvariantCulture);
		}
	}
}