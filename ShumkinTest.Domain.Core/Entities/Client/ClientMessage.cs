using System;
using System.ComponentModel.DataAnnotations;

using ShumkinTest.Domain.Interfaces;

namespace ShumkinTest.Domain.Core.Entities.Client
{
	public class ClientMessage : IEntity, ISyncable
	{
		public ClientMessage()
		{
		}

		public ClientMessage(string message)
		{
			Text = message;
			CreateDate = DateTime.UtcNow;
		}

		public string Text { get; set; }

		public DateTime CreateDate { get; set; }

		[Key]
		public int Id { get; set; }

		public bool IsInvalid(out string message)
		{
			message = "";
			if (!string.IsNullOrEmpty(Text)) return false;

			message = "Message text must be at least 1 symbol!";
			return true;
		}

		public bool IsSync { get; set; }
	}
}