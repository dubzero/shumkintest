using System.ComponentModel.DataAnnotations;
using System.Net;

using NodaTime;

using ShumkinTest.Domain.Interfaces;

namespace ShumkinTest.Domain.Core.Entities.Server
{
	public class ServerMessage : IEntity
	{
		public string Text { get; set; }

		public Instant CreateDate { get; set; }

		public IPAddress IPAddress { get; set; }

		[Key]
		public int Id { get; set; }

		public bool IsInvalid(out string message)
		{
			message = "";
			if (IPAddress != null) return false;

			message = "IPAddress must be set!";
			return true;
		}
	}
}