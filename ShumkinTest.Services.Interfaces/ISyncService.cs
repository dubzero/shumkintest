using System.Linq;
using System.Threading.Tasks;

using ShumkinTest.Domain.Interfaces;

namespace ShumkinTest.Services.Interfaces
{
	public interface ISyncService<T> where T : class, IEntity, ISyncable
	{
		Task StartSync();

		IQueryable<T> GetNotSynced();

		Task<bool> TrySyncAsync(T entity);
	}
}