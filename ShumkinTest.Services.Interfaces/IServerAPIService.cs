using System.Collections.Generic;
using System.Threading.Tasks;

using ShumkinTest.Domain.Interfaces;

namespace ShumkinTest.Services.Interfaces
{
	public interface IServerAPIService<T> where T : class, IEntity, ISyncable
	{
		Task<bool> SendEntity(T entity);

		Task<IEnumerable<IViewModel>> GetEntities();
	}
}