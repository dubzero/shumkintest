FROM mcr.microsoft.com/dotnet/aspnet:5.0-focal AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build

WORKDIR /src
COPY ["ShumkinTest.Server/ShumkinTest.Server.csproj", "ShumkinTest.Server/"]
COPY ["ShumkinTest.Domain.Core/ShumkinTest.Domain.Core.csproj", "ShumkinTest.Domain.Core/"]
COPY ["ShumkinTest.Domain.Interfaces/ShumkinTest.Domain.Interfaces.csproj", "ShumkinTest.Domain.Interfaces/"]
COPY ["ShumkinTest.Infrastructure.Data/ShumkinTest.Infrastructure.Data.csproj", "ShumkinTest.Infrastructure.Data/"]
COPY ["ShumkinTest.Infrastructure.Business/ShumkinTest.Infrastructure.Business.csproj", "ShumkinTest.Infrastructure.Business/"]
COPY ["ShumkinTest.Services.Interfaces/ShumkinTest.Services.Interfaces.csproj", "ShumkinTest.Services.Interfaces/"]

RUN dotnet restore "ShumkinTest.Server/ShumkinTest.Server.csproj"
COPY . .
WORKDIR "/src/ShumkinTest.Server"
RUN dotnet build "ShumkinTest.Server.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ShumkinTest.Server.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ShumkinTest.Server.dll"]