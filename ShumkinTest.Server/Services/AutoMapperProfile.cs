using AutoMapper;

using ShumkinTest.Domain.Core.Entities.Server;
using ShumkinTest.Domain.Core.Extensions;
using ShumkinTest.Domain.Core.Models;

namespace ShumkinTest.Server.Services
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			CreateMap<MessageCreateModel, ServerMessage>()
				.ForMember(p => p.CreateDate, opts => opts.MapFrom(source => NodaTimeExtension.CurrentTime));

			CreateMap<ServerMessage, MessageViewModel>()
				.ForMember(p => p.CreateDate, opts => opts.MapFrom(source => source.CreateDate.InstantToString("dd.MM.yyyy hh:mm:ss")))
				.ForMember(p => p.IPAddress, opts => opts.MapFrom(source => source.IPAddress.ToString()));
		}
	}
}