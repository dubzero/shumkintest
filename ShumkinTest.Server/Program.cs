using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using ShumkinTest.Domain.Core.Entities.Server;
using ShumkinTest.Domain.Core.Models;
using ShumkinTest.Domain.Interfaces;

#pragma warning disable 1998

namespace ShumkinTest.Server
{
	public class Program
	{
		public static async Task Main(string[] args)
		{
			var host = CreateHostBuilder(args).Build();
			var appTask = host.RunAsync(); // Trick for unblock console
			var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			if (env == "Development") // If using docker need to use interactive mode to unblock console input
			{
				await ListenConsole(host.Services);
			}

			await appTask; // Waiting for shutdown
		}

		private static IHostBuilder CreateHostBuilder(string[] args)
		{
			return Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
		}

		private static async Task ListenConsole(IServiceProvider provider)
		{
			await Task.Run(() =>
			{
				while (true)
				{
					var input = ReadLine.Read("Enter command: ");

					if (input == "print")
					{
						try
						{
							using var scope = provider.CreateScope();
							var messageRepo = scope.ServiceProvider.GetRequiredService<IRepository<ServerMessage>>();
							var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();
							var messages = messageRepo.GetListAsync().GetAwaiter().GetResult();
							if (!messages.Any()) Console.WriteLine("No message yet");
							else
							{
								var viewModels = mapper.Map<List<MessageViewModel>>(messages);
								foreach (var message in viewModels)
								{
									message.PrettyWriteConsole();
								}
							}
						}
						catch (Exception ex)
						{
							Debug.Write($"{ex.Message}\n");
						}
					}
					else
					{
						Console.WriteLine("Wrong command");
					}
				}
			});
		}
	}
}