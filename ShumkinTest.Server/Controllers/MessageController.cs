using System.Collections.Generic;
using System.Security;
using System.Threading.Tasks;

using AutoMapper;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using ShumkinTest.Domain.Core.Entities.Server;
using ShumkinTest.Domain.Core.Models;
using ShumkinTest.Domain.Interfaces;

namespace ShumkinTest.Server.Controllers
{
	[ApiController, Route("api/message")]
	public class MessageController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly IRepository<ServerMessage> _serverMessageRepo;

		public MessageController(IMapper mapper, IRepository<ServerMessage> serverMessageRepo)
		{
			_mapper = mapper;
			_serverMessageRepo = serverMessageRepo;
		}

		[HttpGet("all")]
		public async Task<IActionResult> GetAll()
		{
			var messages = await _serverMessageRepo.GetListQuery().ToListAsync();
			var viewModels = _mapper.Map<List<MessageViewModel>>(messages);
			return Ok(viewModels);
		}

		[HttpPost("")]
		public async Task<IActionResult> AddMessage([FromBody] MessageCreateModel model)
		{
			try
			{
				var entity = _mapper.Map<ServerMessage>(model);
				entity.IPAddress = HttpContext.Connection.RemoteIpAddress;

				var result = await _serverMessageRepo.AddAsync(entity);
				if (result)
					return Ok("Message added");
				return BadRequest("Something goes wrong");
			}
			catch (VerificationException ex)
			{
				return BadRequest(ex.Message);
			}
		}
	}
}