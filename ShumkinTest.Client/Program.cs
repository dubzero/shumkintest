﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using ShumkinTest.Domain.Core.Entities.Client;
using ShumkinTest.Domain.Core.Extensions;
using ShumkinTest.Domain.Interfaces;
using ShumkinTest.Infrastructure.Business;
using ShumkinTest.Infrastructure.Data.LocalDb;
using ShumkinTest.Services.Interfaces;

#pragma warning disable 1998

namespace ShumkinTest.Client
{
	internal class Program
	{
		public static void Main(string[] args)
		{
			var builder = ConfigHelper.GetConfigurationBuilder(args);
			var configuration = builder.Build();

			var serviceProvider = new ServiceCollection()
				.AddConfigService(args)
				.AddDbContext(configuration)
				.AddRepositories()
				.AddBusinessLayer()
				.BuildServiceProvider();

			MigrateDb(serviceProvider);

			var syncTimeout = GetSyncTimeout(serviceProvider);

			StartBackgroundSync(serviceProvider, syncTimeout);

			ListenConsole(serviceProvider);

			Console.ReadKey();
		}

		private static async Task PrintCommand(ServiceProvider provider)
		{
			try
			{
				using var scope = provider.CreateScope();
				var apiService = scope.ServiceProvider.GetRequiredService<IServerAPIService<ClientMessage>>();

				var messages = await apiService.GetEntities();

				if (!messages.Any()) Console.WriteLine("No message yet");
				else
				{
					foreach (var message in messages) message.PrettyWriteConsole();
				}
			}
			catch (Exception ex)
			{
				Debug.Write($"{ex.Message}\n");
			}
		}

		private static void CreateAndSyncMessage(ServiceProvider provider, string input)
		{
			try
			{
				using var scope = provider.CreateScope();
				var entity = new ClientMessage(input);
				var messageRepo = scope.ServiceProvider.GetRequiredService<IRepository<ClientMessage>>();
				var syncService = scope.ServiceProvider.GetRequiredService<ISyncService<ClientMessage>>();
				messageRepo.AddAsync(entity).Wait();
				syncService.TrySyncAsync(entity).Wait();
			}
			catch (Exception ex)
			{
				Debug.Write($"{ex.Message}\n");
			}
		}

		private static void ListenConsole(ServiceProvider provider)
		{
			while (true)
			{
				var input = ReadLine.Read("Enter message: ");

				if (input == "print")
				{
					PrintCommand(provider).GetAwaiter().GetResult(); // Sync run to block enter next message
				}
				else if (!string.IsNullOrEmpty(input))
				{
					Task.Run(() =>
					{
						CreateAndSyncMessage(provider, input);
					});
				}
			}
		}

		private static void MigrateDb(ServiceProvider provider)
		{
			using var scope = provider.CreateScope();
			var db = scope.ServiceProvider.GetRequiredService<ClientDbContext>();
			db.Database.Migrate();
		}

		private static void StartBackgroundSync(IServiceProvider provider, int timeout)
		{
			Task.Run(() =>
			{
				while (true)
					try
					{
						using (var scope = provider.CreateScope())
						{
							var syncService = scope.ServiceProvider.GetRequiredService<ISyncService<ClientMessage>>();
							syncService.StartSync().Wait();
						}

						Debug.Write($"Go to sleep for {timeout / 1000} sec\n");
						Thread.Sleep(timeout); // Tries sync every 'timeout ms'. Sleep Thread from ThreadPool, not Main UI Thread
					}
					catch (Exception ex)
					{
						Debug.Write($"{ex.Message}\n");
						Debug.Write($"Go to sleep for {timeout / 1000} sec\n");
						Thread.Sleep(timeout);
					}
			});
		}

		private static int GetSyncTimeout(IServiceProvider provider)
		{
			using (var scope = provider.CreateScope())
			{
				var config = scope.ServiceProvider.GetRequiredService<ClientConfigService>();
				var timeout = config.GetServerConfig().SyncTimeout;
				return timeout ?? 5000; // 5 sec. is default if not set
			}
		}
	}
}