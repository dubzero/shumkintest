using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using ShumkinTest.Domain.Core.Entities.Client;
using ShumkinTest.Domain.Interfaces;
using ShumkinTest.Infrastructure.Business;
using ShumkinTest.Infrastructure.Data.LocalDb;
using ShumkinTest.Infrastructure.Data.LocalDb.Repositories;
using ShumkinTest.Services.Interfaces;

namespace ShumkinTest.Client
{
	public static class DependencyHelper
	{
		public static IServiceCollection AddRepositories(this IServiceCollection services)
		{
			return services
				.AddTransient<IRepository<ClientMessage>, ClientMessageRepo>();
		}

		public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration config)
		{
			return services.AddDbContext<ClientDbContext>(options =>
				options.UseSqlite(config.GetConnectionString("ClientDbConnection"),
					b => { b.MigrationsAssembly("ShumkinTest.Infrastructure.Data"); }));
		}

		public static IServiceCollection AddBusinessLayer(this IServiceCollection services)
		{
			return services
				.AddTransient<ISyncService<ClientMessage>, MessageSyncService>()
				.AddTransient<IServerAPIService<ClientMessage>, MessageSendService>();
		}

		public static IServiceCollection AddConfigService(this IServiceCollection services, string[] args)
		{
			return services
				.AddSingleton(new ClientConfigService(args));
		}
	}
}