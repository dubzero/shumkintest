﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShumkinTest.Domain.Interfaces
{
	public interface IRepository<T>
	{
		IQueryable<T> GetListQuery();

		Task<List<T>> GetListAsync();

		/// <summary>
		///     Save entity and returns success flag
		/// </summary>
		Task<bool> AddAsync(T entity);

		Task<bool> UpdateAsync(T entity);

		Task<T> GetAsync(int id);
	}
}