using System.ComponentModel.DataAnnotations;

namespace ShumkinTest.Domain.Interfaces
{
	public interface IEntity
	{
		[Key]
		int Id { get; set; }

		bool IsInvalid(out string message);
	}
}