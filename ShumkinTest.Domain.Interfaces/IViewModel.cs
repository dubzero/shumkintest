namespace ShumkinTest.Domain.Interfaces
{
	public interface IViewModel
	{
		string GetPrettyView();

		void PrettyWriteConsole();
	}
}