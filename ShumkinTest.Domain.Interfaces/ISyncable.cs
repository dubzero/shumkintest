namespace ShumkinTest.Domain.Interfaces
{
	public interface ISyncable
	{
		bool IsSync { get; set; }
	}
}