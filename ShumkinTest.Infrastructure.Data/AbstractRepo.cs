using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using ShumkinTest.Domain.Interfaces;

namespace ShumkinTest.Infrastructure.Data
{
	public abstract class AbstractRepo<T> : IRepository<T> where T : class, IEntity
	{
		private readonly DbContext _db;

		public AbstractRepo(DbContext db)
		{
			_db = db;
		}

		public virtual IQueryable<T> GetListQuery()
		{
			return _db.Set<T>().AsQueryable();
		}

		public virtual Task<List<T>> GetListAsync()
		{
			return GetListQuery().ToListAsync();
		}

		public virtual async Task<bool> AddAsync(T entity)
		{
			if (entity.IsInvalid(out var message)) throw new VerificationException(message);
			await _db.AddAsync(entity);
			var added = await _db.SaveChangesAsync();
			return added > 0;
		}

		public async Task<bool> UpdateAsync(T entity)
		{
			if (entity.IsInvalid(out var message)) throw new VerificationException(message);
			_db.Update(entity);
			await _db.SaveChangesAsync();
			return true;
		}

		public virtual Task<T> GetAsync(int id)
		{
			return GetListQuery().FirstOrDefaultAsync(p => p.Id == id);
		}

		public virtual async Task<bool> AddRangeAsync(IEnumerable<T> entities)
		{
			foreach (var entity in entities)
				if (entity.IsInvalid(out var message))
					throw new VerificationException(message);

			await using var transaction = await _db.Database.BeginTransactionAsync();
			try
			{
				await _db.AddRangeAsync(entities);
				var added = await _db.SaveChangesAsync();
				if (added == entities.Count())
				{
					await transaction.CommitAsync();
					return true;
				}

				await transaction.RollbackAsync();
				return true;
			}
			catch
			{
				await transaction.RollbackAsync();
				throw;
			}
		}
	}
}