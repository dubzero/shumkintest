using Microsoft.EntityFrameworkCore;

using ShumkinTest.Domain.Core.Entities.Server;

namespace ShumkinTest.Infrastructure.Data.ServerDb
{
	public class ServerDbContext : DbContext
	{
		public ServerDbContext(DbContextOptions<ServerDbContext> options) : base(options)
		{
		}

		public virtual DbSet<ServerMessage> ServerMessages { get; set; }
	}
}