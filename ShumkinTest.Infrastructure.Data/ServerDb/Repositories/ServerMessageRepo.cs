using ShumkinTest.Domain.Core.Entities.Server;

namespace ShumkinTest.Infrastructure.Data.ServerDb.Repositories
{
	public class ServerMessageRepo : AbstractRepo<ServerMessage>
	{
		public ServerMessageRepo(ServerDbContext db) : base(db)
		{
		}
	}
}