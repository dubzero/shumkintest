﻿using System.Net;

using Microsoft.EntityFrameworkCore.Migrations;

using NodaTime;

using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ShumkinTest.Infrastructure.Data.ServerDb.Migrations
{
	public partial class Init : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				"ServerMessages",
				table => new
				{
					Id = table.Column<int>("integer", nullable: false)
						.Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
					Text = table.Column<string>("text", nullable: true),
					CreateDate = table.Column<Instant>("timestamp", nullable: false),
					IPAddress = table.Column<IPAddress>("inet", nullable: true)
				},
				constraints: table => { table.PrimaryKey("PK_ServerMessages", x => x.Id); });
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				"ServerMessages");
		}
	}
}