using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

using ShumkinTest.Domain.Core.Extensions;

namespace ShumkinTest.Infrastructure.Data.LocalDb
{
	public class ClientDbContextFactory : IDesignTimeDbContextFactory<ClientDbContext>
	{
		public ClientDbContext CreateDbContext(string[] args)
		{
			var configuration = ConfigHelper.GetConfigurationBuilder(args).Build();
			var optionsBuilder = new DbContextOptionsBuilder<ClientDbContext>();
			var connectionString = configuration
				.GetConnectionString("ClientDbConnection");

			optionsBuilder.UseSqlite(connectionString);

			return new ClientDbContext(optionsBuilder.Options);
		}
	}
}