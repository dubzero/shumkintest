using Microsoft.EntityFrameworkCore;

using ShumkinTest.Domain.Core.Entities.Client;

namespace ShumkinTest.Infrastructure.Data.LocalDb
{
	public class ClientDbContext : DbContext
	{
		public ClientDbContext(DbContextOptions<ClientDbContext> options) : base(options)
		{
		}

		public virtual DbSet<ClientMessage> ClientMessages { get; set; }
	}
}