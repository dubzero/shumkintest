using System.Threading.Tasks;

using ShumkinTest.Domain.Core.Entities.Client;

namespace ShumkinTest.Infrastructure.Data.LocalDb.Repositories
{
	public class ClientMessageRepo : AbstractRepo<ClientMessage>
	{
		public ClientMessageRepo(ClientDbContext db) : base(db)
		{
		}

		public Task<bool> SetSynced(ClientMessage message)
		{
			message.IsSync = true;
			return UpdateAsync(message);
		}
	}
}