﻿using System;

using Microsoft.EntityFrameworkCore.Migrations;

namespace ShumkinTest.Infrastructure.Data.LocalDb.Migrations
{
	public partial class Init : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				"ClientMessages",
				table => new
				{
					Id = table.Column<int>("INTEGER", nullable: false)
						.Annotation("Sqlite:Autoincrement", true),
					Text = table.Column<string>("TEXT", nullable: true),
					CreateDate = table.Column<DateTime>("TEXT", nullable: false),
					IsSync = table.Column<bool>("INTEGER", nullable: false)
				},
				constraints: table => { table.PrimaryKey("PK_ClientMessages", x => x.Id); });
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				"ClientMessages");
		}
	}
}